# GetPPA.download

> A Personal Package Archive (PPA) is a software repository for uploading source packages to be built and published as an Advanced Packaging Tool (APT) repository by Launchpad.
> While the term is used exclusively within Ubuntu, Launchpad host Canonical envisions adoption beyond the Ubuntu community. [Learn More](https://en.wikipedia.org/wiki/Ubuntu_(operating_system)#Package_Archives)

### Graphics and icons

[IconFinder](https://www.iconfinder.com/icons/6617/package_toys_icon)

### LICENSE

[MII License](https://gitlab.com/tigefa/getppa.download/blob/master/LICENSE)